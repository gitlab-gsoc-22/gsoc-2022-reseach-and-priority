# 👋 Hi there, I'm Siddharth.

🐦 Twitter: [Asthana31](https://twitter.com/Asthana31)

🌏 Website: https://siddharthasthana.dev/

# GSoC 2022 Reseach and Priority

🚨 Week 1 | GSoC Coding Period Starts (13-06-2022 -- 20-06-2022)

- [ ] 1. Git Issues to add mailmap support in `git cat-file` https://gitlab.com/gitlab-org/git/-/issues/106
- [ ] 2. Figured out how `git show` prints the commit object...
    - In the function where revision walk happens, for each commit object a function called `show_log` is called where we eventually print the commit object… it uses `pretty_print` library, specifically the function `pp_header` is where the `author` and `committer` names and `email` are mailmapped…
- [ ] 3. a rough algorithm I come up with:
    - Iterate through each line in the buffer that stores the information about the `commit_object`… I mean this buf https://gitlab.com/gitlab-org/git/-/blob/maint/builtin/cat-file.c#L151
    - if the line starts with author/committer, Use `skip_prefix` and `split_ident_line` function to split the `name`, `timezone` and `date` Information and store in `struct ident_split`
    - use mailmap APIs to get their canonical names and email and replace in the buffer
    - we can use a condition for tagger
- [ ] 4. We can use commit_rewrite_person in revision.h to change the name and email to their canonical version https://gitlab.com/gitlab-org/git/-/blob/maint/revision.c#L3707

- [ ] 5. Completed the first implementation of mailmap in cat-file. The function looks like following:
    ```c
    char* parse_buffer(char* buf)
    {
        struct strbuf buffer = STRBUF_INIT;
        strbuf_addstr(&buffer, buf);
        commit_rewrite_person(&buffer, "\nauthor ", &mailmap);
        commit_rewrite_person(&buffer, "\ncommitter ", &mailmap);
        return buffer.buf;
    }
    ```

- [ ] 6. The first ever code changes in Git for adding mailmap support in cat-file. Here is the link: 
    
    - https://gitlab.com/edith007/git/-/commits/mailmap-support-in-cat-file

- [ ] 7. Here is the Blog for week 1 of GSoC: https://siddharthasthana.dev/blog/gsoc%20week%201%20--%20the%20beginning!/

--- 

🚨 Week 2 | 21-06-2022 -- 28-06-2022

- [ ] 1.
    
